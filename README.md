Installation
============

I recommend using Vundle. To install with it, add the following to your vimrc:

    Plugin 'LucHermitte/lh-vim-lib.git'
    Plugin 'https://gitlab.com/mgoral/vim-project-tags.git'

and then simply call `:VundleInstall`.

Usage
=====

You can use the following commands:

* `:Ctags`: generates tags file for all files in your project.
* `:Ctagsb`: generates tags for current buffer.

Configuration
=============

There are several variables which you can set to customize how tags are
generated:

* `projecttags.ctags.cmd` lets you specify a program which is called when
  generating ctags
* `projecttags.ctags.flags` is a set of flags passed to `projecttags.ctags.cmd`
* `projecttags.ctags.files` are files (or directories) from which ctags will be
  generated.


You can set them like this:

    LetTo g:projecttags.ctags.cmd 'my-ctags'

    LetTo p:projecttags.ctags.files 'src test lib' 

Note that all these variables have some default values.
