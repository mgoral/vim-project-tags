" Buffer relpath according to project root
function! s:projectRelpath(filename)
    let root = lh#project#root()
    let s = substitute(a:filename, root, "", "")
    return s
endfunction

" ctags gen
" [[[
function! s:call_ctags_files(files, ...)
    let cmd = lh#option#get('projecttags.ctags.cmd', 'ctags')
    let flags = lh#option#get('projecttags.ctags.flags', '--exclude=.svn --exclude=.git --exclude=bin --exclude=build --links=no -R')

    let root = fnameescape(lh#project#root())

    if a:0 > 0
        let additional_flags = a:1
    else
        let additional_flags = ''
    endif

    let shell_cmd = cmd. ' ' .flags. ' ' .additional_flags. ' ' .a:files

    if has('nvim')
        " neovim specific async job
        call jobstart(shell_cmd, {'cwd': root})
    else
        if has('job')
            call job_start(['sh', '-c', 'cd ' .root. ' && ' .shell_cmd])
        else
            exe lh#os#system(lh#os#sys_cd(root) . ' && '.shell_cmd)
        endif
    endif
endfunction

function! s:call_ctags()
    let files = lh#option#get('projecttags.ctags.files', '.')
    call s:call_ctags_files(l:files)
endfunction

command! -nargs=0 Ctags call s:call_ctags()
command! -nargs=0 Ctagsb call s:call_ctags_files(s:projectRelpath(expand('%:p')), '--append')
